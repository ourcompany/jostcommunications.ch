﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.XPath;
using System.Net;
using System.Net.Cache;
using System.IO;
using System.Text;

public sealed class TumblrImages
{
    static TumblrImages _instance = null;
    static readonly object padlock = new object();



    public static TumblrImages Instance
    {
        get
        {
            lock (padlock)
            {
                if (_instance == null)
                {
                    _instance = new TumblrImages();
                }
                return _instance;
            }
        }
    }


    #region get data
    public XPathNavigator newsNav
    {
        get
        {
            XPathNavigator res = (XPathNavigator)HttpRuntime.Cache["ch.jostcommunications.TumblrImages"];
            bool success = false;
            string absPath = HttpContext.Current.Server.MapPath("~/App_Data");
            if (res == null)
            {
                res = getNews(DateTime.Now, absPath, out success);

            }
            else
                checkRefresh(absPath);

            return res;
        }
        set
        {
            HttpRuntime.Cache.Insert("ch.jostcommunications.TumblrImages", value);
        }
    }
    private DateTime RefreshDate = DateTime.Now;
    private void checkRefresh(string absPath)
    {

        if (DateTime.Now.Subtract(RefreshDate).Days >= 1)
        {

            refreshDataDelegate del = new refreshDataDelegate(refreshData);
            // sync
            //del.Invoke(DateTime.Now, absPath);
            // async
            del.BeginInvoke(DateTime.Now, absPath, null, null);

        }
    }
    private delegate void IOWriteDelegate(string xmlString, string path);

    private void IOWrite(string xmlString, string path)
    {
        System.IO.File.WriteAllText(path, xmlString);
    }
    private delegate void refreshDataDelegate(DateTime refreshTime, string absPath);

    private void refreshData(DateTime refreshTime, string absPath)
    {

        try
        {
            HttpRuntime.Cache.Remove("ch.jostcommunications.TumblrImages");
        }
        catch
        {
        }
        RefreshDate = refreshTime;
        bool success = false;
        newsNav = getNews(refreshTime, absPath, out success);

    }
    private XPathNavigator getNews(DateTime refreshTime, string absPath, out Boolean success)
    {

        try
        {
            // read num max = 50;
            WebRequest tumblrReq = WebRequest.Create("http://iwdrm.tumblr.com/rss");
            HttpRequestCachePolicy policy = new HttpRequestCachePolicy(HttpRequestCacheLevel.NoCacheNoStore);
            tumblrReq.CachePolicy = policy;
            HttpWebResponse response = (HttpWebResponse)tumblrReq.GetResponse();
            Stream dataStream = response.GetResponseStream();
            string xmlString = new StreamReader(dataStream).ReadToEnd();
            XPathNavigator responseXPATH = new XPathDocument(new StringReader(xmlString)).CreateNavigator();
            if (responseXPATH.Select("rss/channel/item").Count == 0)
                throw new Exception("no posts !" + responseXPATH.OuterXml);
            success = true;
            IOWriteDelegate del = new IOWriteDelegate(IOWrite);
            del.BeginInvoke(xmlString, absPath + "/TumblrImages.xml", null, null);

            return (newsNav = responseXPATH);
        }
        catch (Exception e)
        {
            try
            {
                success = true;

                return new XPathDocument(absPath + "/TumblrImages.xml").CreateNavigator();
            }
            catch
            {
                success = false;
                RefreshDate = refreshTime.AddDays(-1);
                return new XPathDocument(new StringReader("<tumblr></tumblr>")).CreateNavigator();

            }

        }

    }
    #endregion

}