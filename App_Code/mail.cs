﻿#region copyright
// /*
// ******************************************************************
// Copyright (c) 2010, Our Company Ltd.
// All rights reserved.
//             
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// 
// ******************************************************************
// */
#endregion
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;

/// <summary>
///     Handles mail sending
/// </summary>
public static class mail
{
    public static bool SendEmail(StringBuilder htmlEmail, StringBuilder textEmail, string mailto, string subject)
    {
        return SendEmail(htmlEmail.ToString(), textEmail.ToString(), mailto, subject, "");
    }
    public static bool SendEmail(string htmlEmail, string textEmail, string mailto, string subject, string replyTo)
    {
        MailMessage toSend = new MailMessage
                                 {
            From = new MailAddress("post@acuitis.com"),
            Subject = subject,
            Body = textEmail,
            ReplyTo = new MailAddress(string.IsNullOrEmpty(replyTo) ? "post@acuitis.com" : replyTo)
        };

        toSend.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(htmlEmail, new ContentType("text/html")));

        mailto.Split(',').Select(em => em.Trim()).ToList().ForEach(mailAddress => toSend.To.Add(mailAddress));

        SmtpClient mailSending = new SmtpClient {EnableSsl = true};
        try
        {
            mailSending.Send(toSend);
            return true;
        }
        catch
        {
            return false;
        }
    }
}