﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Collections.Generic;
using ourCompany;
using System.Xml.XPath;

/// <summary>
/// Summary description for PageWithMenus
/// </summary>
[PartialCaching(604800, "*", "", "infoAge")]
public class PageBase : System.Web.UI.Page
{
    public string _menu;
    public string _submenu;
    public string _lang;
    public string _title;
    public bool _isLogged; 
    public XPathNavigator _curNode;

    public data _data;

    public PageBase()
    {
        this.Init += new EventHandler(InitPage);
        this.Load += new EventHandler(LoadPage);
        this.LoadComplete += new EventHandler(LoadCompletePage);
    }
    protected void InitPage(object sender, EventArgs e)
    {
        this.EnableViewState = false;
        
        MasterPageBase myMaster = (MasterPageBase)this.Master;
        _menu = myMaster._menu;
        _submenu = myMaster._submenu;
        _lang = myMaster._lang;
        _data = myMaster._data;
        _title = myMaster._title;
    }
    protected void LoadPage(object sender, EventArgs e)
    {
        MasterPageBase myMaster = (MasterPageBase)this.Master;
        _curNode = myMaster._curNode;
    }
    protected void LoadCompletePage(object sender, EventArgs e)
    {
        this.Page.Title = _title;
    }

}
