﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Collections.Generic;
using ourCompany;
using System.Xml.XPath;

/// <summary>
/// Summary description for PageWithMenus
/// </summary>
[PartialCaching(604800, "*","","infoAge")]
public class MasterPageBase : System.Web.UI.MasterPage
{
    public string _menu;
    public string _lang;
    public string _submenu;
    public bool _isLogged;

    public XPathNavigator _curNode;
    public string _title = "jostcommunications.ch";

    public data _data;
    public MasterPageBase()
    {
        this.Init += new EventHandler(InitPage);
    }
    protected void InitPage(object sender, EventArgs e)
    {
        assets.create(this.Page);
        _menu = Request.Params["menu"] ?? "";
        _submenu = Request.Params["submenu"] ?? "";
        _lang = Request.Params["lang"];
        if (!data.languages.Instance.LanguageList.AllKeys.Any(s => s == _lang))
            _lang = cultureFinder.getLanguageString(Request, data.languages.Instance.LanguageList.AllKeys.ToList());

        _data = data.languages.Instance[_lang];
    }
    
   
}