﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ourCompany;

/// <summary>
/// Summary description for Visitor
/// </summary>

public sealed class Visitor
{
    const string SESSION_SINGLETON = "Visitor";


    private int _maxImages;
    private int _curImage;
    private Visitor()
    {
        init();
    }
    public void init()
    {
        _usedImages = new List<int>();
        _maxImages = data.languages.Instance[data.languages.Instance.LanguageList.AllKeys.First()].getElements("root/content/credit/gif").Count;
        
        int count = _maxImages;
        while (count-- > 0)
        {
            _usedImages.Add(count);
        }
        _usedImages = new List<int>(_usedImages.OrderBy(a => Guid.NewGuid()));
        _curImage = 0;
    }
    public static Visitor Instance
    {
        get
        {
            Visitor _instance;
            if (null == System.Web.HttpContext.Current.Session[SESSION_SINGLETON])
            {
                _instance = new Visitor();
                System.Web.HttpContext.Current.Session[SESSION_SINGLETON] = _instance;
            }
            else
                _instance = (Visitor)System.Web.HttpContext.Current.Session[SESSION_SINGLETON];

            return _instance;
        }
    }
    private List<int> _usedImages;
    public int nextImage()
    {
        if(_curImage >= _maxImages)
            init();
        if (_curImage >= _usedImages.Count)
            return 0;
        return _usedImages[_curImage];
    }
    public void useImage()
    {
        _curImage++;
    }
}