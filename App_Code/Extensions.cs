﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Specialized;

/// <summary>
/// Summary description for Extensions
/// </summary>
public static class Extensions
{
    public delegate void NameValueCollectionDelegate(string key, string value);
    public static void Each(this NameValueCollection collection, NameValueCollectionDelegate del)
    {
        foreach (string key in collection.AllKeys)
        {
            del(key, collection[key]);
        }
    }
    public delegate void NameValueCollectionDelegateLast(string key, string value, bool isLast);
    public static void Each(this NameValueCollection collection, NameValueCollectionDelegateLast del)
    {
        for (var i = 0; i < collection.Count; i++)
        {
            del(collection.AllKeys[i], collection[i], i == (collection.Count - 1));
        }
        
    }
	
}