﻿var site = {
    domready: function () {
        new lightbox();
        if (Browser.Platform.ios || Browser.Platform.android)
            document.title = "jostcomm";
    },
    call: function (action, args) {
        this.fireEvent(action, args);
        return void (0);
    }
};
Object.append(site, new Events());
window.addEvent("domready", function () { site.domready(); });