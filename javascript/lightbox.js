﻿var lightbox = new Class({
    _parentContainerId: "container",
    _call: function (e) {

        switch (e.a) {
            case "open":
                this._open(e);
                break;
        }
    },
    initialize: function () {
        site.addEvent("lightbox", function (e) { this._call(e); } .bind(this));
    },
    _init: function (e) {

        var container = $(e.c);
        var lightBoxContainer = new Element("div", { "class": "lightboxFullHeight", styles: { "opacity": 0 }, events: { "click": function (ev) { this._close(e); } .bind(this)} });
        var lightboxwarp = new Element("div", { "class": "lightboxWarp" });
        container.store("lightBoxContainer", lightBoxContainer);
        var size = window.getSize();
        var contentContainer =
            new Element("div", { "class": "lightboxContent" }).adopt(new Element("img", { src: e.src + "?maxwidth=" + (size.x - 20) + "&maxheight=" + (size.y - 20) }));

        $(document.body).adopt(
            lightBoxContainer
            .adopt(
                lightboxwarp
                .adopt(
                    new Element("div", { "class": "lightboxTable", events: { "click": function (ev) { this._close(e) } .bind(this)} }).adopt(
                        new Element("div", { "class": "lightboxCell", events: { "click": function (ev) { this._close(e) } .bind(this)} }).adopt(
                                contentContainer
                        )
                    )
                )
            )
        );
        container.store("contentContainer", lightboxwarp);
    },
    _open: function (e) {
        var container = $(e.c);
        if (!container.retrieve("lightbox")) {
            this._init(e);
            var fx = new Fx.Tween(container.retrieve("lightBoxContainer"), { property: "opacity" });
            if (Browser.Platform.ios || Browser.Platform.android)
                fx.addEvent("complete", this._ipadPosition.pass(e.c, this));
            container.store("lightbox", fx);
            fx.start(1);
        }
        else {
            var fx = container.retrieve("lightbox");
            fx.cancel();
            fx.start(1);
        }
    },
    _ipadPosition: function (e) {

        var c = $(e).retrieve("contentContainer");
        var top = $(document.body).getScroll().y;
        var left = $(document.body).getScroll().x;
        c.setStyles({
            "height": $(window).getSize().y,
            "width": "100%",
            "position": "absolute",
            "top": top,
            "left": left
            
        });



        //var mTop = (- $(document.body).getScrollSize().y + $(document.body).getSize().y)/2 +  $(document.body).getScroll().y;

        //$(e).retrieve("contentContainer").setStyle("margin-top", mTop.toInt());
    },
    _close: function (e) {
        var container = $(e.c);
        container.retrieve("lightbox").start(0).chain(function () { container.retrieve("lightBoxContainer").destroy(); container.store("lightBoxContainer", false); container.store("lightbox", false); });
    }
});