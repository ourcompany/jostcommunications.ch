﻿using System;
using System.Xml.XPath;
using ourCompany;

public partial class _Default : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        _title += " - Blog";
        string pageNumStr = Request.Params["page"];
        int pageNum;
        if (!int.TryParse(pageNumStr, out pageNum))
            pageNum = 1;

        if (_curNode == null)
            _curNode = _data.getElement("root/content/blog");

        
        //if (string.IsNullOrEmpty(_menu))
        //    _menu = "blog";

        new CSSControl()
            .Render("div.col_left", false)
                .Render("div.col_left_title", false)
                    .Render("div.col_left_table", false)
                        .Render("div.col_left_cell", _curNode.GetAttribute("title"))
                    .closeTag()
                .closeTag()
                .Render("div.textLeft", _curNode.GetInnerXml())
            .closeTag()
            .Render("div.col_right", false)
                .Render("div.col_right_text", false)
                    .Apply(c =>
                    {
                        double postsCount = (double)_curNode.Evaluate("count(post)");

                        double divRes = postsCount / 7.0;
                        double maxPage = Math.Ceiling(divRes);
                        XPathNodeIterator posts = _curNode.Select("post[position() > " + ((pageNum - 1) * 7) + " and position() <= " + (pageNum * 7) + "]");
                        posts.Each((post, i) =>
                        {

                            c
                                .Render("div.post", false)
                                    .Render("div.title", post.GetAttribute("title"), false)
                                        .Render("span.date", " | " + post.GetAttribute("date"))
                                    .closeTag()
                                    .Apply(p =>
                                    {
                                        post.Select("paragraph|image|video").Each(panel =>
                                        {
                                            switch (panel.Name)
                                            {
                                                case "paragraph":
                                                    p
                                                        .Render("div.paragraphTitle", panel.GetAttribute("title"))
                                                        .Render("div.blogText", panel.GetInnerXml());
                                                    break;
                                                case "image":
                                                    if (panel.GetAttribute("type") == "two")
                                                    {
                                                        p
                                                            .Render("div.twoImages", false)
                                                            .AddAttribute("src", panel.GetAttribute("src2") + "?width=215")
                                                            .Render("img.right")
                                                            .AddAttribute("src", panel.GetAttribute("src1") + "?width=215")
                                                            .Render("img.left")
                                                            .Render("div.clear", "<!-- -->")
                                                            .closeTag();
                                                    }
                                                    else
                                                    {
                                                        p
                                                            .Render("div.oneImage", false)
                                                            .AddAttribute("src", panel.GetAttribute("src1") + "?width=450")
                                                            .Render("img")
                                                            .closeTag();
                                                    }
                                                    break;
                                                case "video":
                                                    c
                                                            .Render("div.video", false);
                                                    switch (panel.GetAttribute("uses"))
                                                    {
                                                        case "youtube":
                                                            c
                                                                .AddAttribute("type", "text/html")
                                                                .AddAttribute("width", "450")
                                                                .AddAttribute("height", panel.GetAttribute("vidHeight"))
                                                                .AddAttribute("src", "http://www.youtube.com/embed/" + panel.GetAttribute("videoYOUTUBEID"))
                                                                .Render("iframe.youtube-player");
                                                            break;
                                                        case "vimeo":
                                                            c
                                                                .AddAttribute("type", "text/html")
                                                                .AddAttribute("width", "450")
                                                                .AddAttribute("height", panel.GetAttribute("vidHeight"))
                                                                .AddAttribute("src", "http://player.vimeo.com/video/" + panel.GetAttribute("videoVIMEOID") + "?title=0&byline=0&portrait=0")
                                                                .Render("iframe.vimeo-player");
                                                            break;
                                                    };
                                                    c.closeTag();
                                                    break;

                                            }
                                        });
                                    })
                                .closeTag(); // close post
                        });
                        c.Render("div.pageControls", false);
                        if (maxPage > 1)
                        {
                            if (pageNum > 1)
                            {
                                c
                                    .AddAttribute("href", "/" + _lang + "/blog/" + (pageNum - 1))
                                    .Render("a.prev", "<");
                            }
                            c
                                .Apply(cc =>
                                {
                                    for (var i = 1; i <= maxPage; i++)
                                    {
                                        if (pageNum == i)

                                            cc
                                                .AddAttribute("href", "/" + _lang + "/" + _menu + "/" + i.ToString())
                                                .Render("a.Page.curPage", i.ToString());
                                        else
                                            cc
                                                .AddAttribute("href", "/" + _lang + "/" + _menu + "/" + i.ToString())
                                                .Render("a.Page", i.ToString());
                                    }
                                });

                            if (pageNum < maxPage)
                            {
                                c
                                    .AddAttribute("href", "/" + _lang + "/blog/" + (pageNum + 1))
                                    .Render("a.next", ">");

                            }


                            c.closeTag();
                        }
                    })
                .closeTag()
            .closeTag()
        .AppendToPlaceHolder(contentPH);


    }

}