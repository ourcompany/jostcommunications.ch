﻿<%@ Application Language="C#" %>
<script RunAt="server">

    void Application_Start(object sender, EventArgs e)
    {
        // Code that runs on application startup
    }

    void Application_End(object sender, EventArgs e)
    {
        //  Code that runs on application shutdown

    }

    void Application_Error(object sender, EventArgs e)
    {
        // Code that runs when an unhandled error occurs

    }

    void Session_Start(object sender, EventArgs e)
    {
        // Code that runs when a new session is started

    }

    void Session_End(object sender, EventArgs e)
    {
        ourCompany.cms.SessionHandler.sessionStateModule_End(Session);
    }
    public virtual string GetVaryByCustomString(HttpContext context, string custom)
    {
        if (custom == "infoAge")
        {
            return ourCompany.data.Instance.Version;
        }
        return base.GetVaryByCustomString(context, custom);
    }
</script>
