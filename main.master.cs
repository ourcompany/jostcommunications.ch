﻿using System;
using System.Web.UI;
using ourCompany;
using System.Xml.XPath;
using System.IO;
using ourCompany;


public partial class main : MasterPageBase
{

    protected void Page_Init(object sender, EventArgs e)
    {
        new CSSControl()
            .Render("div.langs", false)
                .Apply(c =>
                {
                    data.languages.Instance.LanguageList.Each((k, v, last) =>
                    {

                        c
                            .Apply(k != _lang, cc =>
                            {
                                string href = "/" + k +
                                    (!string.IsNullOrEmpty(_menu) ? ("/" + _menu) : "") +
                                    (!string.IsNullOrEmpty(_submenu) ? ("/" + _submenu) : "") +
                                    (!string.IsNullOrEmpty(Request.Params["lbl"]) ? ("/" + Request.Params["lbl"]) : "") +
                                    (!string.IsNullOrEmpty(Request.Params["page"]) ? ("/" + Request.Params["page"]) : "");
                                cc
                                    .AddAttribute("href", href);
                            }, cc =>
                            {
                                cc
                                    .AddAttribute("href", "#");
                            })
                            .Render("a.lang" + (k != _lang ? "" : ".active"), v)
                            .Apply(!last, cc => cc.Render("span.sep", "|"));

                    });
                })
            .closeTag()
            .AppendToPlaceHolder(languagesPH);
        new CSSControl()
            .Apply(c =>
            {
                XPathNodeIterator menuNodes = _data.getElements("root/content/*");
                while (menuNodes.MoveNext())
                {
                    if (_menu == menuNodes.Current.Name)
                    {
                        _curNode = menuNodes.Current.Clone();
                        c.AddAttribute("href", "/" + _lang + "/" + menuNodes.Current.Name)
                         .Render("a.m.active", menuNodes.Current.GetAttribute("title"));
                    }
                    else
                    {
                        c.AddAttribute("href", "/" + _lang + "/" + menuNodes.Current.Name)
                         .Render("a.m", menuNodes.Current.GetAttribute("title"));
                    }

                }
            })
            .AppendToPlaceHolder(menuPH);

        XPathNavigator footerNode = _data.getElement("root/footer");
        new CSSControl()
            .Write(footerNode.GetInnerXml())
            .AppendToPlaceHolder(footerPH);

        google.create(_data.getElement("root/config"), this.Page, analyticsPH);
        string iconPng = "";
        if (!string.IsNullOrEmpty(iconPng = _data.getElement("root/config").GetAttribute("appleTouchIcon")))
            head.Controls.Add(new LiteralControl("<link rel='apple-touch-icon' href='" + iconPng + "' />"));
    }

}
