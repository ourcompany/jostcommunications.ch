﻿using System;
using System.Xml.XPath;
using ourCompany;

public partial class _Default : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (_curNode == null)
            _curNode = _data.getElement("root/credit");

        _title += " - " + _curNode.GetAttribute("title");

        XPathNavigator TumblrNav = TumblrImages.Instance.newsNav;
        new CSSControl()
            .Render("div.creditTop", false)
                .Render("div.TumblrImage", false)
                    .Apply(c =>
                    {
                        // find the entry : 
                        XPathNavigator imageNode = null;
                        int crazy = 100;
                        
                        while (crazy > 0 && (imageNode = _curNode.SelectSingleNode("gif[" + Visitor.Instance.nextImage() + "]")) == null)
                        {
                            Visitor.Instance.init();
                            crazy--;
                        }
                        if(imageNode == null)
                            return;
                        Visitor.Instance.useImage();
                        //int itemCount = (int)(double)TumblrNav.Evaluate("count(rss/channel/item)");
                        c
                            .Render("div.image", false)
                                .AddAttribute("src",imageNode.GetAttribute("src"))
                                .Render("img")
                                .Render("div.quote", imageNode.GetAttribute("quote"))
                                .Render("div.title", imageNode.GetAttribute("title"))
                            .closeTag();

                            //Server.HtmlDecode(TumblrNav.SelectSingleNode("rss/channel/item[" + (new Random().Next(1, itemCount + 1)) + "]/description").InnerXml)
                            //);
                    })
                .closeTag()
                .Render("div.col_left", _curNode.GetInnerXml("toptext"))
                .Apply(c =>
                {
                    
                    c
                        .AddAttribute("href", "?" + Visitor.Instance.nextImage())
                        .Render("a#nextCredit", "next");

                })
                .Render("div.clear", "<!-- -->")
            .closeTag()
            .Render("div.clear", "<!-- -->")
            .Render("div.creditCols.clearfix", false)
                .Render("div.col_left", false)
                    .Render("div.textLeft", _curNode.GetInnerXml())
                .closeTag()
                .Render("div.col_right", false)
                    .Apply(c =>
                    {
                        _curNode.Select("image").Each((n, i) =>
                        {
                            c
                                .AddAttribute("href", "javascript:site.call('lightbox', {a : 'open', c : 'light_item_" + i + "', src : '" + n.GetAttribute("src") + "'});")
                                .Render("a#light_item_" + i + ".imageLink", false)
                                    .AddAttribute("src", n.GetAttribute("src") + "?maxwidth=500")
                                    .Render("img")
                                .closeTag();
                        });
                    })
                .closeTag()
            .closeTag()
            .AppendToPlaceHolder(contentPH);


    }

}