﻿using System;
using System.Xml.XPath;
using ourCompany;

public partial class _Default : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Random rd = new Random();
        int preventCacheImg = rd.Next(2048);
        if (_curNode == null)
            _curNode = _data.getElement("root/contact");

        _title += " - " + _curNode.GetAttribute("title");

        XPathNavigator TumblrNav = TumblrImages.Instance.newsNav;
        new CSSControl()
            .Render("div.contactTop.clearfix",false)
                .AddStyleAttribute("min-height", "1px")
                .Render("div.col_left", "<!-- -->")
                .Render("div.col_right", false)
                    .AddAttribute("src", "http://www.webcam-basel.ch/warteck/webcam.jpg?" + preventCacheImg.ToString())
                    .Render("img.cam")
                    .Render("div.camlegend", false)
                        .Render("a[href=http://www.webcam-basel.ch/;target=_blank]", "webcam")
                        .Write(" by ")
                        .Render("a[href=http://www.zweihochdrei.ch/;target=_blank]", "zweihochdrei")
                    .closeTag()
                .closeTag()
            .closeTag()
            .Render("div.contactBottom.clearfix", false)
                .Render("div.col_left", false)
                    .AddAttribute("href", "mailto:jost@jostcommunications.ch")
                    .Render("a[target=_blank]#mail", "<!-- -->")
                    .AddAttribute("href", _curNode.GetAttribute("facebook"))
                    .Render("a[target=_blank]#facebook", "<!-- -->")
                    .AddAttribute("href", _curNode.GetAttribute("twitter"))
                    .Render("a[target=_blank]#twitter", "<!-- -->")
                    .AddAttribute("href", "/" + _lang + "/impressum")
                    .Render("a#impressumLink", ((_lang == "de") ? "Impressum" : "Imprint"))
                .closeTag()
                .Render("div.col_right", false)
                    .Render("div.googleMap", false)
                    .Write(_curNode.GetInnerXml("google"))
                    .closeTag()
                .closeTag()
            .closeTag()
            .Render("div.clear", "<!-- -->")
            .AppendToPlaceHolder(contentPH);


    }

}