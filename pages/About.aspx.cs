﻿using System;
using System.Xml.XPath;
using ourCompany;

public partial class _Default : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (_curNode == null)
            _curNode = _data.getElement("root/about");

        _title += " - " + _curNode.GetAttribute("title");


        new CSSControl()
            .Render("div.col_left", false)
                .Render("div.col_left_title", false)
                    .Render("div.col_left_table", false)
                        .Render("div.col_left_cell", _curNode.GetAttribute("leftTitle"))
                    .closeTag()
                .closeTag()
                .Render("div.textLeft", _curNode.GetInnerXml())
            .closeTag()
            .Render("div.col_right", false)
                .Render("div.col_right_text", false)
                    .Apply(!string.IsNullOrEmpty(_curNode.GetAttribute("rightTitle")), cc => cc.Render("div.title", _curNode.GetAttribute("rightTitle")))
                    .Apply(c =>
                    {
                        _curNode.Select("paragraph|image|list").Each(item =>
                        {
                            c
                                .Render("div.panel", false);
                            switch (item.Name)
                            {
                                case "paragraph":
                                    c
                                        
                                        .Render("div.blogText", item.GetInnerXml());
                                    break;
                                case "list":
                                    c
                                        .Render("div.Title14", item.GetAttribute("title"))
                                        .Render("div.listText", false)
                                            .Apply(cc =>
                                                {
                                                    item.Select("listItem").Each(li => {
                                                        cc
                                                            .Render("div.listItem.clearfix", false)
                                                                .Render("div.listBullet", "•")
                                                                .Render("div.listContent", li.GetInnerXml())
                                                            .closeTag();
                                                    });
                                                })
                                        .closeTag();
                                    break;
                                case "image":
                                    if (item.GetAttribute("type") == "two")
                                    {
                                        c
                                            .Render("div.twoImages", false)
                                                .AddAttribute("src", item.GetAttribute("src2") + "?width=215")
                                                .Render("img.right")
                                                .AddAttribute("src", item.GetAttribute("src1") + "?width=215")
                                                .Render("img.left")
                                                .Render("div.clear", "<!-- -->")
                                            .closeTag();
                                    }
                                    else
                                    {
                                        c
                                            .Render("div.oneImage", false)
                                                .AddAttribute("src", item.GetAttribute("src1") + "?width=450")
                                                .Render("img")
                                            .closeTag();
                                    }
                                    break;
                            }
                            c
                                .closeTag();
                        });
                    })
                .closeTag()
            .closeTag()
            .AppendToPlaceHolder(contentPH);


    }

}