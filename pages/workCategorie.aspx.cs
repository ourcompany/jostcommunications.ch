﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.XPath;
using ourCompany;

public partial class pages_workCategorie : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        


        if (_curNode == null)
            _curNode = _data.getElement("root/works/categorie[@label = '" + _submenu + "']");
        else
        {
            _title += " - " + _curNode.GetAttribute("title");
            _curNode = _curNode.SelectSingleNode("categorie[@label = '" + _submenu + "']");
        }

        _title += " - " + _curNode.GetAttribute("title");

        new CSSControl()
            .Render("div#workCategorie", false)
                .Render("div.col_left", false)
                    .Render("div.col_left_title", false)
                        .Render("div.col_left_table", false)
                            
                            .Render("span.col_left_cell", _curNode.GetAttribute("title"))
                        .closeTag()
                    .closeTag()
                .closeTag()
                .Render("div.col_right", false)
                    .Render("div.col_right_text", false)
                        .Apply(c =>
                        {
                            _curNode.Select("work").Each((work, i) =>
                            {
                                if(i % 2 == 1) 
                                    c.AddStyleAttribute("margin-right", "20px");
                                c
                                    .AddStyleAttribute("background-image", "url(" + work.GetAttribute("src") + "?crop=auto&width=220&height=210)")
                                    .AddAttribute("href","/" + _lang + "/works/" + _submenu + "/" + work.GetAttribute("label"))
                                    .Render("a.workContainer", false)
                                        .Render("span.legend", work.GetAttribute("title"))
                                    .closeTag();

                                if (i % 2 == 0)
                                    c.Render("div.clear", "<!-- -->");
                            });
                            c.Render("div.clear", "<!-- -->");
                        })
                    .closeTag()
                .closeTag()
                .Apply(c =>
                {
                    XPathNavigator nextCategorie = _curNode.SelectSingleNode("following-sibling::categorie");
                    if (nextCategorie == null)
                        nextCategorie = _curNode.SelectSingleNode("..").SelectSingleNode("categorie");

                    c
                        .AddAttribute("href", "/" + _lang + "/works/" + nextCategorie.GetAttribute("label"))
                        .Render("a#nextWork", "next");

                })
            .closeTag()
            .AppendToPlaceHolder(contentPH);
    }
}
