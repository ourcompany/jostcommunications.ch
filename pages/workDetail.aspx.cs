﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.XPath;
using ourCompany;

public partial class pages_workDetail : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        string workLabel = Request.Params["lbl"];

        XPathNavigator categorieNode;

        if (_curNode == null)
            categorieNode = _data.getElement("root/works/categorie[@label = '" + _submenu + "']");
        else
        {
            _title += " - " + _curNode.GetAttribute("title");
            categorieNode = _curNode.SelectSingleNode("categorie[@label = '" + _submenu + "']");
        }
        _title += " - " + categorieNode.GetAttribute("title");

        XPathNavigator workNode = categorieNode.SelectSingleNode("work[@label = '" + workLabel + "']");
        _title += " - " + workNode.GetAttribute("title");

        new CSSControl()
            .Render("div#workDetail.clearfix", false)
                .Render("div.col_left", false)
                    .Render("div.col_left_title", false)
                        .Render("div.col_left_table", false)
                            .Render("div.col_left_cell", workNode.GetAttribute("title"))
                        .closeTag()
                    .closeTag()
                    .Render("div.textLeft", _curNode.GetInnerXml())
                .closeTag()
                .Render("div.col_right", false)
                    .Render("div.col_right_text", false)
                        .Apply(c =>
                        {
                            workNode.Select("description/paragraph|description/image|description/video|description/audio").Each((item, i) =>
                            {
                                c
                                    .Render("div.panel", false);
                                switch (item.Name)
                                {
                                    case "paragraph":
                                        c
                                            .Render("div.blogText", item.GetInnerXml());
                                        break;
                                    case "video":
                                        c
                                                .Render("div.video", false);
                                        switch (item.GetAttribute("uses"))
                                        {
                                            case "youtube":
                                                c
                                                    .AddAttribute("type", "text/html")
                                                    .AddAttribute("width", "450")
                                                    .AddAttribute("height", item.GetAttribute("vidHeight"))
                                                    .AddAttribute("src", "http://www.youtube.com/embed/" + item.GetAttribute("videoYOUTUBEID"))
                                                    .Render("iframe.youtube-player");
                                                break;
                                            case "vimeo":
                                                c
                                                    .AddAttribute("type", "text/html")
                                                    .AddAttribute("width", "450")
                                                    .AddAttribute("height", item.GetAttribute("vidHeight"))
                                                    .AddAttribute("src", "http://player.vimeo.com/video/" + item.GetAttribute("videoVIMEOID") + "?title=0&byline=0&portrait=0")
                                                    .Render("iframe.vimeo-player");
                                                break;
                                        };
                                        c.closeTag();
                                        break;
                                    case "audio" :
                                        c.Render("div.audio", false)
                                            .AddAttribute("controls","controls")
                                            .AddAttribute("autobuffer", "autobuffer")
                                            .AddAttribute("preload", "auto")
                                            .Render("audio", false)
                                                .AddAttribute("src", item.GetAttribute("srcMp3"))
                                                .AddAttribute("type", "audio/mpeg")
                                                .Render("source")
                                                .AddAttribute("src", item.GetAttribute("srcOgg"))
                                                .AddAttribute("type", "audio/ogg")
                                                .Render("source")
                                                .AddAttribute("href", item.GetAttribute("srcMp3"))
                                                .Render("a", "download mp3 file")
                                            .closeTag()
                                        .closeTag();
                                        break;
                                    case "image":

                                        if (item.GetAttribute("type") == "two")
                                        {
                                            c
                                                .Render("div.twoImages.clearfix", false)
                                                    .AddAttribute("href", "javascript:site.call('lightbox', {a : 'open', c : 'light_item_" + i + "_1', src : '" + item.GetAttribute("src2") + "'});")
                                                    .Render("a#light_item_" + i + "_1", false)
                                                        .AddAttribute("src", item.GetAttribute("src2") + "?width=215")
                                                        .Render("img.right")
                                                    .closeTag()
                                                    .AddAttribute("href", "javascript:site.call('lightbox', {a : 'open', c : 'light_item_" + i + "_2', src : '" + item.GetAttribute("src1") + "'});")
                                                    .Render("a#light_item_" + i +"_2", false)
                                                        .AddAttribute("src", item.GetAttribute("src1") + "?width=215")
                                                        .Render("img.left")
                                                    .closeTag()
                                                .closeTag();
                                        }
                                        else
                                        {
                                            c
                                                .Render("div.oneImage", false)
                                                    .AddAttribute("href", "javascript:site.call('lightbox', {a : 'open', c : 'light_item_" + i + "', src : '" + item.GetAttribute("src1") + "'});")
                                                    .Render("a#light_item_" + i , false)
                                                        .AddAttribute("src", item.GetAttribute("src1") + "?width=450")
                                                        .Render("img")
                                                    .closeTag()
                                                .closeTag();
                                        }
                                        break;
                                }
                                c
                                    .closeTag();
                            });
                        })
                    .closeTag()
                .closeTag()
                .Apply(c =>
                {
                    XPathNavigator nextWork = workNode.SelectSingleNode("following-sibling::work");
                    if (nextWork == null)
                        nextWork = categorieNode.SelectSingleNode("work");

                    c
                        .AddAttribute("href","/" + _lang + "/works/" + _submenu + "/" + nextWork.GetAttribute("label"))
                        .Render("a#nextWork", "next");

                })
            .closeTag()
            .AppendToPlaceHolder(contentPH);
    }
}
