﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.XPath;
using ourCompany;

public partial class pages_Work : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        

        if (_curNode == null)
            _curNode = _data.getElement("root/works");
        
        _title += " - " + _curNode.GetAttribute("title");

        new CSSControl()
            .Render("div.work", false)
            .Render("div.workCategories", false)
                .Apply( c => {
                    _curNode.Select("categorie").Each(categorie =>
                    {
                        c
                            .Render("div.catTitle", false)
                                .Render("div.catTable", false)
                                    .AddAttribute("href", "/" + _lang + "/works/" + categorie.GetAttribute("label"))
                                    .Render("a.catLink", categorie.GetAttribute("title"))
                                .closeTag()
                            .closeTag();
                    });
                })
            .Render("div.clear")
            .closeTag()
            .closeTag()
            .AppendToPlaceHolder(contentPH);
        
    }
}
