﻿using System;
using System.Xml.XPath;
using ourCompany;

public partial class _Default : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Random rd = new Random();
        int preventCacheImg = rd.Next(2048);
        _curNode = _curNode.SelectSingleNode("impressum");
        
        _title += " - " + _curNode.GetAttribute("title");

        new CSSControl()
            .Render("div#impressum", _curNode.GetInnerXml())
            .AppendToPlaceHolder(contentPH);


    }

}